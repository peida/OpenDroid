package org.loader.opendroid;

import org.loader.opendroid.db.OpenDroid;

public class Student extends OpenDroid {
	private String stuName;
	private int stuAge;
	private String stuAddr;

	public String getStuAddr() {
		return stuAddr;
	}

	public void setStuAddr(String stuAddr) {
		this.stuAddr = stuAddr;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public int getStuAge() {
		return stuAge;
	}

	public void setStuAge(int stuAge) {
		this.stuAge = stuAge;
	}
}
